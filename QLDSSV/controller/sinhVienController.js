function layThongTinTuForm() {
  let ma = document.getElementById("txtMaSV").value;
  let ten = document.getElementById("txtTenSV").value;
  let email = document.getElementById("txtEmail").value;
  let hinhAnh = document.getElementById("txtImg").value;

  return {
    ten: ten,
    email: email,
    hinhAnh: hinhAnh,
    ma: ma,
  };
}

function showThongTinLenForm(data) {
  document.getElementById("txtTenSV").value = data.ten;
  document.getElementById("txtEmail").value = data.email;
  document.getElementById("txtImg").value = data.hinhAnh;
  document.getElementById("txtMaSV").value = data.ma;
}
