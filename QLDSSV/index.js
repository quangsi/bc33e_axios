var dssv = [];
var BASE_URL = "https://62f8b7523eab3503d1da1597.mockapi.io";

var batLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
var tatLoading = function () {
  document.getElementById("loading").style.display = "none";
};

var renderTable = function (list) {
  var contentHTML = "";
  list.forEach(function (item) {
    var trContent = `
   <tr>
    <td>${item.ma}</td>
    <td>${item.ten}</td>
    <td>${item.email}</td>
    <td>
       <img src=${item.hinhAnh} style="width:80px"   alt="" />
    </td>
    <td>
      <button
      onclick="xoaSinhVien('${item.ma}')"
      class="btn btn-danger">Xoá</button>


      <button
      onclick="suaSinhVien('${item.ma}')"
      class="btn btn-warning">Sửa</button>
    </td>
  </tr>
    `;
    contentHTML += trContent;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

var renderDssvService = function () {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      dssv = res.data;
      renderTable(dssv);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
};
renderDssvService();
function xoaSinhVien(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      tatLoading();
      // chạy lại renderDssvService để render danh sách mới nhất
      renderDssvService();
      console.log(res);
    })
    .catch(function (err) {
      tatLoading();

      console.log(err);
    });
}

function themSV() {
  var dataForm = layThongTinTuForm();
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: dataForm,
  })
    .then(function (res) {
      tatLoading();
      renderDssvService();
      console.log(res);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function suaSinhVien(id) {
  console.log("id: ", id);
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      console.log(res);
      tatLoading();
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}
function capNhatSV() {
  var dataForm = layThongTinTuForm();
  console.log("dataForm: ", dataForm);
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${dataForm.ma}`,
    method: "PUT",
    data: dataForm,
  })
    .then(function (res) {
      tatLoading();
      renderDssvService();
      console.log(res);
      // response
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

// pending , resolve ,reject
